public class ForexClass 
{
	@InvocableMethod(label = 'Update currency' description = 'Update currency value' )
	public static list<forex__c> updateForex(list<forex__C> fobjList)
    {
        String url='http://data.fixer.io/api/latest?access_key=';
        String key='4223b91a7f1ef0aae5712db337d93871';
        decimal result=0.0;
        try
        {
        	HTTP h= new HTTP();
        	HTTPRequest req = new HTTPRequest();
        	req.setEndpoint(url+key);
        	req.setMethod('GET');
        
        	HTTPResponse res = h.send(req);
        
        	map<string, object> jsonBody = (map<string,object>)Json.deserializeUntyped(res.getBody());
        	Map<string,object> rates = (Map<string,object>) jsonBody.get('rates'); 
        
        	updateCurrency(rates);
        }
        catch(Exception e)
        {
            System.debug('Error is : '+e);
        }
        return fobjList;
    }
    
    public static void updateCurrency(Map<string,object> rates)
    {
        list<forex__c> forexListUpdated = new list<forex__c>();
        try
        {
        	list<forex__c> oldforexlist = [select id, country__c, currency_value_for_1_EUR__c from forex__c];
        
        	for(forex__c f : oldforexlist)
        	{
            	decimal val = ((decimal) rates.get(f.country__c)*1.0);
            	f.Currency_value_for_1_EUR__c = val;
            	forexlistupdated.add(f);
        	}
        	update forexListUpdated;
        }
        catch(Exception e)
        {
            System.debug('Error is : '+e);
        }
    }
   
}