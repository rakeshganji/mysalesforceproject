@isTest(seeAllData=True)
public class ForexClassTest 
{
    @isTest
    static void updateForexTest()
    {
        list<forex__c> flist = [select id, country__c, currency_value_for_1_EUR__c from forex__c where country__c in ('INR','USD','EUR')];
        list<forex__c> getflist = new list<forex__c>();
        Test.startTest();
        getflist = ForexClass.updateForex(flist);
        Test.stopTest();
    }
    
    @isTest
    static void updateCurrencyTest()
    {
        map<string,object> rate = new map<string,object>();
        rate.put('AED',4.7 );
        rate.put('AFN',104.65);
        rate.put('INR', 87.395242);
        rate.put('EUR', 1);
        
        Test.startTest();
        ForexClass.updateCurrency(rate);
        list<forex__c> flistAfterUpdate = [select id, country__c, currency_value_for_1_EUR__c from forex__c where country__c in ('EUR','INR')];
        System.assertEquals(1.00000, flistAfterUpdate[0].currency_value_for_1_EUR__c, 'EUR currency mismatch');
        System.assertEquals(87.395242, flistAfterUpdate[1].currency_value_for_1_EUR__c, 'INR currency mismatch');
    	Test.stopTest();
    }
}