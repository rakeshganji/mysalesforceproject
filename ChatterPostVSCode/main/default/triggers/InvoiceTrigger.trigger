trigger InvoiceTrigger on APEX_Customer__c (after update)
{
    list<apex_invoice__c> invlist = new list<apex_invoice__c>();
    for(APEX_Customer__c cus : Trigger.new)
    {
        if(cus.apex_customer_status__c=='Active' && trigger.oldmap.get(cus.id).apex_customer_status__c != 'Active')
        {
            apex_invoice__c inv = new apex_invoice__c();
            inv.apex_customer__c = cus.id;
            inv.apex_status__c='Pending';
            inv.apex_description__c='Invoice created through trigger';
            invlist.add(inv);
        }
    }
    insert invlist;
}